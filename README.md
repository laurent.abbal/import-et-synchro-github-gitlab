# Import et synchro GitHub - GitLab

## Terminologie GitHub / GitLab

| GITHUB          | GITLAB    |
| ---             | ---       |
| `organisations` | `groups`  |
| `repository`    | `project` |
| `gist`          | `snippet` |

## Importer un `repository` GitHub dans GitLab

*Temps : 5 minutes*

* Sur GitLab, crér un nouveau projet : https://forge.apps.education.fr/projects/new
* Choisir "Import project"
* Cliquer sur le bouton "GitHub"

`forge.apps.education.fr` n'est pas configuré pour permettre une connection  directe via GitHub. L'utilisation d'un "Personnal Acces Token" est donc nécessaire. L'utilisation d'un "Personnal Acces Token" n'est pas la méthode recommandée mais c'est la seule possible actuellement.

Message : "Note: Consider asking your GitLab administrator to configure GitHub integration, which will allow login via GitHub and allow importing repositories without generating a Personal Access Token."

* Pour créer un "Personal Access Token" GitHub, aller à l'adresse : https://github.com/settings/tokens/new
* Donner une description ("GitLabImport" par exemple)
* Choisir une date d'expiration
* Choisir le champ d'application: "repo" (ou laisser vide pour importer uniquement des projets publics)
* Cliquer sur "Generate token"
* Copier la clé (il ne sera pas possible de la récupérer plus tard)
* Revenir sur GitLab, copier la clé dans le champ et cliquer sur "Authenticate"
* Attendre un peu que la liste s'affiche
* Choisir un `repository`/`project` à importer, modifier les information dans la colonne "To GitLab" si nécessaire puis cliquer sur "Import"

Documentation : https://docs.gitlab.com/ee/user/project/import/github.html

## Synchroniser de GitHub vers GitLab

*Temps : 5 minutes*

* Créer un "Personal Access Token" GitLab en allant à l'adresse : https://forge.apps.education.fr/-/profile/personal_access_tokens
* Donner une description ("GitHubSync" par exemple)
* Choisir une date d'expiration ou laisser le champ vide
* Choisir le champ d'application: "api"
* Cliquer sur "Create a personnal access token"
* Copier la clé (il ne sera pas possible de la récupérer plus tard)
* Aller dans le `repository` GitHub qui sera synchronisé sur GitLab
* Cliquer sur "Settings" puis sur "Secrets"puis sur "Actions"
* Cliquer sur le bouton "New repository secret"
* Choisir un nom ("GITLAB_TOKEN" par exemple), coller la clé dans le champ puis cliquer sur "Add secret"
* Cliquer ensuite sur l'onglet "Actions" puis sur le bouton "New workflow"
* Cliquer sur le lien "set up a workflow yourself"
* Choisir un nom pour le fichier YML ("gitlabsync.yml" par exemple)
* Coller le code ci-dessous et modifier `target-url` et `target-username`

```yml
name: forgeedu-sync

on:
  - push
  - delete

jobs:
  sync:
    runs-on: ubuntu-latest
    name: Git Repo Sync
    steps:
    - uses: actions/checkout@v2
      with:
        fetch-depth: 0
    - uses: wangchucheng/git-repo-sync@v0.1.0
      with:
        target-url: https://forge.apps.education.fr/chemin_vers_projet
        target-username: nom_utilisateur_forgeedu
        target-token: ${{ secrets.GITLAB_TOKEN }}
```


* Puis cliquer sur "Start commit"

Et voilà! A partir de maintenant, toute modification faite dans le répertoire GitHub sera automatiquement envoyée sur GitLab. Pas d'action manuelle nécessaire. Le déclenchement se fait dès qu'il y a un `commit`.

Remarque : si vous utilisez une `organisation` sur GitHub vous pouvez enregistrer "GITLAB_TOKEN" dans l'organisation puis l'utiliser pour tous les `repository` de l'organisation.

## Synchroniser de GitLab vers GitHub

*Temps : 5 minutes*

Lire https://docs.gitlab.com/ee/user/project/repository/mirror/index.html#pushing-to-a-remote-repository-core et choisir la méthode `push`. Note : la méthode `pull` n'est actuellement pas disponible sur `forge.apps.education.fr`.
